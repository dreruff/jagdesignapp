# Greg Ruffin
# 9/19/14
# working tested

class Exercise

  # Assume that "str" is a sequence of words separated by spaces.
  # Return a string in which every word in "str" that exceeds 4 characters is replaced with "marklar".
  # If the word being replaced has a capital first letter, it should instead be replaced with "Marklar".
  def self.marklar(str)
    temp = str.scan(/\w+/)
    temp.each_index{ |i|
    	if temp[i].length > 4
    		if temp[i].capitalize == temp[i]
    			str = str.gsub(temp[i], "Marklar")
    		else
    			str = str.gsub(temp[i], "marklar")
    		end
    	end
    }
    return str
  end

  # Return the sum of all even numbers in the Fibonacci sequence, up to
  # the "nth" term in the sequence
  # eg. the Fibonacci sequence up to 6 terms is (1, 1, 2, 3, 5, 8),
  # and the sum of its even numbers is (2 + 8) = 10
  def self.even_fibonacci(nth)
    sum = 0
    
    if nth == (1 || 2)
    	return sum
    end
    
    firstTerm = secondTerm = 1
    i = 3
    while i <= nth
    	temp = firstTerm + secondTerm
    	
    	if temp%2 == 0
    		sum += temp
    	end
    	
    	firstTerm = secondTerm
    	secondTerm = temp 
    	i += 1
    end
    
    return sum
  end

end
